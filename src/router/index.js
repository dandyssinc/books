import Vue from 'vue'
import VueRouter from 'vue-router'
import MainPage from '../views/MainPage.vue'
import SearchPage from '@/views/SearchPage.vue'
import FavoritesPage from '@/views/FavoritesPage.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'MainPage',
    component: MainPage,
    children: [
      {
        path: '',
        name: 'searchPage',
        component: SearchPage
      },
      {
        path: 'favorites',
        name: 'favoritesPage',
        component: FavoritesPage //component: () => import(/* webpackChunkName: "about" */ '@/views/FavoritesPage.vue')
      }

    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
