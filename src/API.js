import axios from 'axios';
import {PER_PAGE} from '@/consts.js';

export const HTTP = axios.create({
    baseURL: 'https://www.googleapis.com/books/v1',
    params: {
      langRestrict: 'ru',
      maxResults: PER_PAGE
    },
    headers: {
      'X-Requested-With': 'XMLHttpRequest'
    },
    timeout: 15000
  });

  export const Routes = {
    volumes: '/volumes',
  };
