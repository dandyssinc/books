import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  created() {
    // запускаем мутацию для вытягивания данных из localstorage
		this.$store.commit('initialiseStore');
	},
  mounted() {
    // для записи state favorites в localStorage
    this.$store.subscribe((mutation, state) => {
	    localStorage.setItem('favorites', JSON.stringify(state.favorites));
    });
  },
  render: h => h(App)
}).$mount('#app')
