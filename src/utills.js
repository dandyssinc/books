export function throttle(func, ms) {

  let isThrottled = false,
    savedArgs,
    savedThis;

  function wrapper() {

    if (isThrottled) { // (2)
      savedArgs = arguments;
      savedThis = this;
      return;
    }

    func.apply(this, arguments); // (1)

    isThrottled = true;

    setTimeout(function() {
      isThrottled = false; // (3)
      if (savedArgs) {
        wrapper.apply(savedThis, savedArgs);
        savedArgs = savedThis = null;
      }
    }, ms);
  }

  return wrapper;
}

// export function throttle1 (callback, limit) {
//   var wait = false;
//   return function () {
//       if (!wait) {
//           callback.call();
//           wait = true;
//           setTimeout(function () {
//               wait = false;
//           }, limit);
//       }
//   }
// }

export function getScrollHeight() {
  const scrollHeight = Math.max(
    document.body.scrollHeight, document.documentElement.scrollHeight,
    document.body.offsetHeight, document.documentElement.offsetHeight,
    document.body.clientHeight, document.documentElement.clientHeight
  );
  return scrollHeight;
}

export function getScrollTop() {
  const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
  return scrollTop;
}