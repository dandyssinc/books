import Vue from 'vue';
import Vuex from 'vuex';
import {HTTP, Routes} from '@/API.js';
import {PER_PAGE, PER_PAGE_FOR_MOBILE} from '@/consts.js';

Vue.use(Vuex)

const initState = () => ({
  searchName: '',
  listBooks: [],
  favorites: [],
  listHints: [],
  totalItems: 0,
  perPage: PER_PAGE,
  currentPage: 1,
  currentPageFavorite: 1,
  countFavoritesItemsMobile: PER_PAGE_FOR_MOBILE,
  currentBook: null,
  searchByAuthor: false,
  wasSearch: false,
  visibleSearch: true,
  visibleDetailMobile: false
});

export default new Vuex.Store({

  state: initState(),

  getters: {
    getIsFavorite(state) {
      const isFind = state.favorites.find(el => el.id === state.currentBook.id)
      return Boolean(isFind);
    }
  },
  mutations: {
    /**
     * Мутация для установки state favorites из localStorage
     */
		initialiseStore(state) {
			if(localStorage.getItem('favorites')) {
        const favorites = JSON.parse(localStorage.getItem('favorites'));
        state.favorites = [...favorites];
			}
		},
    setFavorites(state, value) {
      const itemIndex = state.favorites.findIndex(el => el.id === value.id);
      if (itemIndex >= 0) {
        state.favorites.splice(itemIndex,1);
      }
      else {
        state.favorites.push(value);
      }
    },
    setListHints(state, items) {
      if (items) {
        for(let el of items) {
          const find = state.listHints.find(item => (item.id === el.id) || (item.volumeInfo.title === el.volumeInfo.title));
          if(!find) {
            state.listHints.push(el);
          }
        }
      }
    },
    clearListHints(state) {
      state.listHints = [];
    },
    setCountFavoritesItemsMobile(state, value) {
      state.countFavoritesItemsMobile = value;
    },
    setWasSearch(state, value) {
      state.wasSearch = value;
    },
    setVisibleDetailMobile(state, value) {
      state.visibleDetailMobile = value;
    },
    setVisibleSearch(state, value) {
      state.visibleSearch = value;
    },
    setSearchByAuthor(state, value) {
      state.searchByAuthor = value;
    },
    setNameSearch(state, value) {
      state.searchName = value;
    },
    setCurrentBook(state, value) {
      state.currentBook = {...value};
      state.visibleDetailMobile = true;
    },
    setCurrentPage(state, value) {
      state.currentPage = value;
    },
    setCurrentPageFavorite(state, value) {
      state.currentPageFavorite = value;
    },
    setListBooksData(state, {items, totalItems}) {
      state.listBooks = items ?? [];
      state.totalItems = totalItems;
    },
    setListBooksMobileData(state, {items, totalItems}) {
      
      if (items) {
        for(let el of items) {
          const find = state.listBooks.find(item => item.id === el.id);
          if(!find) {
            state.listBooks.push(el);
          }
        }
      }

      state.totalItems = totalItems;
    }
  },
  actions: {
    // https://stackoverflow.com/questions/7266838/google-books-api-returns-json-with-a-seemingly-wrong-totalitem-value бага в определении общего количества элементов.
    getBooksList({state, commit}, payload) {
      if(!state.searchName) {return};
      return new Promise((resolve, reject) => {
        const findBy = (payload && payload.findBy) ? `${payload.findBy}:` : state.searchByAuthor ? 'inauthor:' : 'intitle:';
        const perPage = payload.isMobile ? PER_PAGE_FOR_MOBILE : state.perPage;
        const params = {
          params: {
            q: findBy === 'inauthor:' ? `${findBy}"${state.searchName}"` : findBy+state.searchName,
            startIndex: payload && payload.page ? perPage*(payload.page - 1) : '0',
            maxResults: payload && payload.maxResults ? payload.maxResults : state.perPage
          }
        };
        HTTP.get(Routes.volumes, params)
          .then((response) => {
            if (payload && payload.isMobile) {
              commit('setListBooksMobileData', response.data);

            }
            else {
              commit('setCurrentPage', payload.page);
              commit('setListBooksData', response.data);
              commit('setVisibleSearch', false);
            }
            commit('setWasSearch', true);
            resolve(response.data);
          })
          .catch(e => {
            console.error(e);
            reject(e);
          })
      })
    },

    getListHints({state, commit}, payload) {
      return new Promise((resolve, reject) => {

        const params = {
          params: {
            q: `intitle:${state.searchName}`,
            maxResults: 40
          }
        }

        HTTP.get(Routes.volumes, params)
          .then(({data}) => {


            commit('setListHints', data.items);
            resolve();
          })
          .catch((e) => {
            reject();
            console.error(e);
          })
          
      })
    }
  },

})
